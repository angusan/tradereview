## 券商交易紀錄視覺化
- Parsing 券商匯出的交易紀錄，產生對應的 power language 語法
- 以 multicharts 顯示進出場點位，方便進行交易檢討

### Reminder:
- 為了方便撰寫和除錯，僅以 this bar close 顯示進出概況
- 價位非並精準，但應可做交易檢討的程度


